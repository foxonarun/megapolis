﻿using Backend;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts.Devscreen
{

    #region One row in list
    [System.Serializable]
    public class SelectorRow
    {
        [Tooltip("String name of enum BackendType")]
        public string StringBackendTypeId;
        public GameObject RowObj;

        [System.NonSerialized]
        public Toggle CheckBox;

        [System.NonSerialized]
        public bool isInitialized;

        [System.NonSerialized]
        public BackendType BackendTypeId;



        Action<SelectorRow, bool> OnSelected;
        
        void OnValueChanged(bool isOn)
        {
            OnSelected?.Invoke(this, isOn);
        }


        public bool Init(Action<SelectorRow, bool> listener)
        {
            if (string.IsNullOrEmpty(StringBackendTypeId))
                StringBackendTypeId = RowObj.gameObject.name;

            if (!Enum.TryParse(StringBackendTypeId.Trim(), out BackendTypeId))
                Debug.LogError(" wrong typeid '" + StringBackendTypeId + "'");
            else
                CheckBox = RowObj.GetComponentInChildren<Toggle>();



            if (CheckBox != null)
            {
                OnSelected = listener;
                CheckBox.onValueChanged.AddListener(OnValueChanged);
                isInitialized = true;
            }
            return isInitialized;
        }

        public TMP_InputField GetUrlField()
        {
            if (!isInitialized)
                return null;
            return RowObj.transform.GetComponentInChildren<TMP_InputField>();
        }

    }
    #endregion

    public class ServerSelectorPanel : MonoBehaviour
    {
        [Tooltip("Scene name to run")]
        public string NextSceneName;

        [Tooltip("Parent of all checkboxes")]
        public GameObject ServerSelectorObject;

        public Button StartButton;

        [Header("Setup toggles using strng names of enum BackendType")]
        public List<SelectorRow> Selectors;
        

        [Header("Public for debug purposes only")]
        public BackendConfigData Data;

        ToggleGroup toggleGroup;


        bool DoUpdateUi = true;

        ScreenOrientation initialOrientation;

        private void Awake()
        {
            initialOrientation = Screen.orientation;

            if (initialOrientation != ScreenOrientation.Portrait)
                Screen.orientation = ScreenOrientation.Portrait;
        }

        // Start is called before the first frame update
        void Start()
        {


            toggleGroup = GetComponentInChildren<ToggleGroup>();
            Selectors.ForEach(el => el.Init(OnValueChanged));

            StartButton.onClick.AddListener(DoStartApplication);
            BackendConfig.Load();
            Data = BackendConfig.Data;

            if (ServerSelectorObject == null)
                ServerSelectorObject = gameObject;

            BackendConfig.MarkAsCustomBuild();
        }


        void DoStartApplication()
        {

            Save();
            SceneManager.LoadScene(NextSceneName);
        }


        void Save()
        {
            BackendConfig.UpdateData(CurrentRow.BackendTypeId);
            if (CurrentRow.BackendTypeId == BackendType.Custom)
                BackendConfig.CustomURL = (CurrentRow.GetUrlField().text.Trim());
            BackendConfig.Save();
        }

        SelectorRow CurrentRow = null;
        void UpdateUi()
        {
            foreach (var el in Selectors)
            {
                if (el.isInitialized)
                    el.GetUrlField().text = BackendConfig.GetUrlByBackendType(el.BackendTypeId);
            }

            if (CurrentRow == null)
                CurrentRow = Selectors.Find(el => el.isInitialized && el.BackendTypeId == Data.CurBackendTypeId);

            if (CurrentRow == null)
                CurrentRow = Selectors[0];

            CurrentRow.CheckBox.isOn = true;
            toggleGroup.NotifyToggleOn(CurrentRow.CheckBox);
            BackendConfig.Save();
        }


        void OnValueChanged(SelectorRow row, bool isOn)
        {
           // Debug.Log("On value changed "+ row.RowObj.name+ " to "+isOn);
            if (isOn)
            {
                CurrentRow = row;
                Save();
            }
            else if(row.BackendTypeId == BackendType.Custom)
            {
                BackendConfig.CustomURL = (row.GetUrlField().text.Trim());
                Save();
            }
            
        }

        // Update is called once per frame
        void Update()
        {
            if (DoUpdateUi)
            {
                DoUpdateUi = false;
                UpdateUi();
            }
        }

        private void OnDestroy()
        {
            if (Screen.orientation != initialOrientation)
                Screen.orientation = initialOrientation;
        }
    }
}
