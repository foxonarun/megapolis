﻿using Common;
using UnityEngine;

namespace DataClasses
{

    [System.Serializable]
    public class DataClassBase<T> : IStringId
        where T: DataClassBase<T>, new() 
    {
        virtual public string StringId => throw new System.NotImplementedException();

        public string To() { return JsonUtility.ToJson(this, true); }
        public bool IsSucceed { get; protected set; }

        public T NewFrom(string jsonStr)
        {
            T ret = null;
            ret = JsonUtility.FromJson<T>(jsonStr);
            ret.IsSucceed = true;

            return ret;
        }

        public void Overwrite(string jsonStr)
        {
            JsonUtility.FromJsonOverwrite(jsonStr,(T) this);
            this.IsSucceed = true;
        }
    }
}
