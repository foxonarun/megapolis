﻿using System;

namespace DataProvider
{
    /*
     *     
     * TODO: add list for group operations like resources cleanup an then calling garbage collector.
     * See DeepFreeResources.
     *  registration in this list has to be done automatical, by calling method from DataObject constructors.
     *  Otherwise too easy to forgot something
     *
     */

    /// <summary>
    /// Static class for DataProvider services and static referencies to data objects
    /// </summary>
    public static partial class DataProviderSys
    {


        static public  event Action AnyUpdate = delegate { };
        static void AnyUpdateInvoke(bool isuccess) => AnyUpdate.Invoke();



    }


}


/*
public class DataProviderEvent
{
    public event Action updateEv = delegate { };
    public void Subscribe(Action onUpdateHandler)
    {
        updateEv += onUpdateHandler;
    }
    public void UnSubscribe(Action onUpdateHandler)
    {
        updateEv -= onUpdateHandler;
    }

    public void Invoke()
    {
        updateEv?.Invoke();
    }

}
*/