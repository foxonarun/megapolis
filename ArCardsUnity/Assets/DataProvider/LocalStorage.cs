﻿using DataClasses;
using UnityEngine;

namespace DataProvider
{
    public static class LocalStorage
    {

        static public T Read<T>()
            where T:DataClassBase<T>,new()
        {
            T obj = new T();
            if ( ! PlayerPrefs.HasKey( obj.StringId ) ) return obj;

            string str = PlayerPrefs.GetString(obj.StringId, null);
            if (str != null)
                obj.Overwrite(str);
            return obj;
        }

        static public bool Read<T>(T obj  )
            where T : DataClassBase<T>, new()
        {
            if ( ! PlayerPrefs.HasKey( obj.StringId ) ) return false;

            string str = PlayerPrefs.GetString(obj.StringId, null);
            if ( str != null )
            {
                obj.Overwrite(str);
                return true;
            }

            return false;
        }

        static public void ClearFor( string key )
        {
            PlayerPrefs.DeleteKey( key );
        }

        static public void Save<T>(T obj, bool flush = true)
        where T : DataClassBase<T>, new()
        {
            PlayerPrefs.SetString(obj.StringId, obj.To());
            if (flush)
                PlayerPrefs.Save();

        }


    }
}
