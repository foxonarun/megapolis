﻿/*
namespace DataProvider
{



    public class FavoritePlacesDataObject : DataObjectBase
    {

        #region Implementation of abstract methods

        protected override BaseRequest CreateLoadRequest(ResponseAction onResponseLoadHandler)
        {
            GetSavedPlacesRequest newReq = new GetSavedPlacesRequest();
            // no continuation token
            newReq.Setup(HTTPMethods.Get, onResponseLoadHandler, "", DataProviderSys.SessionId);
            return newReq;

        }

        /// <summary>
        /// do nothing here - the initialization for OnDataChanged(IsLoaded); is done in base constructor
        /// </summary>
        /// <param name="permanentUpdateHandler"></param>
        public FavoritePlacesDataObject(Action<bool> permanentUpdateHandler) : base(permanentUpdateHandler)
        {
        }

        /// <summary>
        /// heavy clear, has to be done at least on city switch 
        /// </summary>
        public override void DeepFreeResources()
        {
            IsLoaded = false;
            Data.Clear();
        }
        #endregion Implementation of abstract methods


        override protected bool ParseLoadedData(string jsonString)
        {
            bool rc = false;

            try
            {
                var response = JsonUtility.FromJson<GetSavedPlacesRequest.Response_SC>(jsonString);
                List<OnePlaceData> newData = new List<OnePlaceData>();
                Debug.Log("Parsed " + JsonUtility.ToJson(response, true));

                foreach (var el in response.Result)
                {
                    var OnePlaceData = new OnePlaceData
                    {
                        Id = el.id,
                        Name = el.Name,
                        GeoCoordinates = new LatLongAltitude(el.Latitude, el.Longitude, el.Altitude)
                    };

                    newData.Add(OnePlaceData);
                  //  Debug.Log(OnePlaceData.DumpToString());
                }
                Data = newData;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            return rc;
        }


        #region  support for events
        event ExtResponseAction OnGotRespPlaceDeleted = delegate { };
        public void SubscribeOnDeletions(ExtResponseAction handler)
        {
            OnGotRespPlaceDeleted += handler;
        }

        public void UnSubscribeOnDeletions(ExtResponseAction handler)
        {
            OnGotRespPlaceDeleted -= handler;
        }
        #endregion


        #region Data and operations with them
        /// <summary>
        /// Warning! do not "cache" it to local variable, use directly!
        /// </summary>
        public List<OnePlaceData> Data { get; private set; } = new List<OnePlaceData>();


        public OnePlaceData FindPlace(LatLong point)
        {
            return Data?.Find(el => el != null && el.IsClose(point));
        }

        public LatLongAltitude GetLatLongAltitude(int index)
        {
            if (index < 0 || index >= Data.Count)
                return ConfigManager.instance.GetDefaultMapArea().GetLatLongAltitude();

            return Data[index].GeoCoordinates;
        }
        #endregion



        #region delete place
        void OnPlaceDeletedCallback(string jsonString, bool isSuccess, string errMsg, ReqInfo reqInfo)
        {


            OnGotRespPlaceDeleted(jsonString, isSuccess, errMsg, reqInfo);
            if (isSuccess && !string.IsNullOrEmpty(reqInfo.GetValue(PlaceIdKey)))
            {
                List<OnePlaceData> newData = new List<OnePlaceData>(Data);
                string placeId = reqInfo.GetValue(PlaceIdKey);
                newData.RemoveAll(el => el != null && el.IsSameId(placeId));
                Data = newData;
            }

            InvokeOnDataChanged(isSuccess);
        }


        public const string PlaceIdKey = "PlaceId";
        public void DeleteOnePlaceAtServer(string savedPlaceID)
        {
            if (string.IsNullOrEmpty(savedPlaceID))
                return;
            // if (RequestOptimization.instance.CheckRequestStatus ("DeleteSavePlace")) return;
            //RequestOptimization.instance.AddRequest ("DeleteSavePlace");

            ProfileStruct tmp = new ProfileStruct();

            DeleteSavedPlaceRequest newReq = new DeleteSavedPlaceRequest();
            newReq.Setup
                (
                    HTTPMethods.Post,
                    OnPlaceDeletedCallback,
                    savedPlaceID,
                    ConfigManager.instance.session_Id,
                    tmp
                 );

            newReq.AddReqKeyValue(PlaceIdKey, savedPlaceID);
            newReq.Send();
            //newReq.Send(true,"{}");

            // ServerHelper.SendRequest(newReq);

            //ServerHelper.SendPostExtRequest<DeleteSavedPlaceRequest>(OnPlaceDeletedCallback, savedPlaceID, ConfigManager.instance.session_Id, tmp);

        }

        #endregion deleteplace

        #region Save place to server
        public void SavePlace(string placeName, string buildingId)
        {
            ReqSavePlaceStruct reqStruct = ReqSavePlaceStruct.CreateReqSavePlaceStruct(placeName, buildingId);

            ServerHelper.SendPostRequest<SavePlaceRequest>(OnSavePlaceResponse, reqStruct);

        }

        private void OnSavePlaceResponse(string resp, bool success, string response)
        {
            //InvokeOnDataChanged(success);
            LoadData();

            if (success)
            {
                JsonData savePlaceResp = JsonMapper.ToObject(resp);
                try
                {
                    if (savePlaceResp["Result"] != null && (string)savePlaceResp["Result"] == "Success")
                    {
                        UIManger.ShowMessage("Place has been added to your favorites");
                    }
                }
                catch (System.Exception err)
                {
                    UIManger.ShowMessage("Favorite already exist");
                }
            }
        }
        #endregion


    }




}

    */