﻿#if false
namespace Scipts.Scene_Map.FavoritePlaces
{
    public struct ReqSavePlaceStruct
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double X { get; set; }
        public double Z { get; set; }
        public double Altitude { get; set; }
        public double SeaAltitude { get; set; }
        /*
                public float Latitude { get; set; }
                public float Longitude { get; set; }
                public float X { get; set; }
                public float Z { get; set; }
                public float Altitude { get; set; }
                public float SeaAltitude { get; set; }
                */

        public string BuildingId { get; set; }
        public string CustomName { get; set; }
        public string SessionId { get; set; }


        public static ReqSavePlaceStruct CreateReqSavePlaceStruct(string placeName, string buildingId)
        {
            LatLongAltitude wallLocation = WallData.instance.selectedWallLocation;
            var pinPosition = Api.Instance.CameraApi.GeographicToWorldPoint(
        LatLongAltitude.FromDegrees(
            wallLocation.GetLatitude(),
            wallLocation.GetLongitude(),
            wallLocation.GetAltitude()));



            ReqSavePlaceStruct reqStruct = new ReqSavePlaceStruct();

            reqStruct.Latitude = (float)wallLocation.GetLatitude();
            reqStruct.Longitude = (float)wallLocation.GetLongitude();
            reqStruct.X = pinPosition.x;
            reqStruct.Z = pinPosition.z;



            reqStruct.Altitude = (float)wallLocation.GetAltitude();
            reqStruct.BuildingId = (buildingId ?? "25.12345,55.12345");

            reqStruct.CustomName = (!string.IsNullOrEmpty(placeName) ?
                placeName :
                "GPS:" + reqStruct.Latitude + "," + reqStruct.Latitude);

            reqStruct.SessionId = ConfigManager.instance.session_Id;

            return reqStruct;
        }

    }

}
#endif