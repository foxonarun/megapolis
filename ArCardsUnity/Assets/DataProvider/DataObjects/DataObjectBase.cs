﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if false
namespace DataProvider
{
    abstract public class DataObjectBase
    {
        /// <summary>
        ///      T newReq = new T();
        /// </summary>
        /// <param name="onLoadResponseHandler"></param>
        /// <returns></returns>
        abstract protected BaseRequest CreateLoadRequest(ResponseAction onLoadResponseHandler);

        /// <summary>
        /// deep clear all cached data, possible with call Garbage collection and resource cleaning,
        /// </summary>
        abstract public void DeepFreeResources();

        protected DataObjectBase() { }

        public DataObjectBase(Action<bool> permanentUpdateHandler)
        {

            if (permanentUpdateHandler != null)
                OnDataChanged += permanentUpdateHandler;
            
        }
        

        

        protected event Action<bool> OnDataChanged = delegate { };
        protected void InvokeOnDataChanged(bool isSuccess)
        {
            OnDataChanged(isSuccess);
        }

        /// <summary>
        /// subscribe to updates
        /// </summary>
        /// <param name="eventHandler">OnUpdated handler, should be called on OnEnable of Monobehavior </param>
        public void Subscribe(Action<bool> eventHandler)
        {
            OnDataChanged += eventHandler;
        }

        /// <summary>
        /// unsubscribe from updates, should be called on Ondisable of Monobehaviour
        /// </summary>
        /// <param name="eventHandler"></param>
        public void UnSubscribe(Action<bool> eventHandler)
        {
            OnDataChanged -= eventHandler;
        }

        protected abstract bool ParseLoadedData(string jsonString);
        public string ErrorText { get; private set; }

        protected void OnLoadResponse(string resp, bool success, string error)
        {
            ErrorText = error;
            IsLoaded = success && ParseLoadedData(resp);
            LastAttemptToLoad = DateTime.UtcNow;
            OnDataChanged(IsLoaded);
            System.GC.Collect();
        }



        public bool IsLoaded { get; protected set; }
        public DateTime LastAttemptToLoad { get; protected set; } = DateTime.UtcNow.AddYears(-10);

        protected const float hour = 60 * 60;
        /// <summary>
        /// by default time of any data expiration is 30 min
        /// </summary>
        public float ExpirationTime { get; protected set; } = 0.5f* hour;

        public virtual bool IsExpired { get { return !IsLoaded || (DateTime.UtcNow - LastAttemptToLoad).TotalSeconds > ExpirationTime; } }


        public virtual void LoadData()
        {
            BaseRequest newReq = CreateLoadRequest(OnLoadResponse);
            newReq.Send();
        }

    }
}
#endif