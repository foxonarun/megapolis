﻿using System.Collections;
using System.Collections.Generic;
using Common;
using DataClasses;
using UnityEngine.Networking;
using System;
using UnityEngine;

namespace Requests
{
    [System.Serializable]
    public class UrlVariable
    {
        public string Name;
        public string Val;
    }

    [System.Serializable]
    public class Galery
    {
        public string Id;
        public string URL;
    }


    [System.Serializable]
    public class ReqConfigurationResp:DataClassBase<ReqConfigurationResp>
    {
        public List<UrlVariable> UrlVariables;
        public List<StringIdVal> Galeries;
    }

    public enum CustomReqErrors
    {
        NoError = 0,
        AlreadyRunning,
        NotInitialized

    }

    abstract public class ReqBase<TResultData> : System.IDisposable
        where TResultData : new()
    {
        /// <summary>
        /// will be added to base URL
        /// </summary>
        public abstract string endpoint { get; }
        public TResultData ResultData;
        protected UnityWebRequest webRequest = null;

        public virtual void Dispose()
        {
            if (webRequest != null)
                webRequest.Dispose();
        }

        public bool IsRunning { get; protected set; } = false;

        protected Action<long, string> OnErrorDelegate;
        void OnError(long errId,string errorText)
        {
            string errMsg = errorText + " errId="+errId+" Request type = " + GetType() + " for endpoint " + endpoint;
            Debug.LogError("errMsg");
            OnErrorDelegate?.Invoke(errId, errorText);
        }

        IEnumerator Execute()
        {
            
            if (IsRunning)
            {
                OnError((int)CustomReqErrors.AlreadyRunning, "Attempt to start Execute() while it's already running");
                yield break;
            }

            if (webRequest == null)
            {
                OnError((int)CustomReqErrors.NotInitialized, "Was not initialized");
                yield break;
            }

            IsRunning = true;
            yield return null;
            yield return webRequest.SendWebRequest();

            
            if (webRequest.isHttpError || webRequest.isNetworkError )
            {
                IsRunning = false;
                OnError(webRequest.responseCode, webRequest.error);
            }

            IsRunning = false;
        }

        /// <summary>
        /// Setup the responce
        /// </summary>
        /// <param name="baseURL">should not have "/" at the end</param>
        abstract public void Setup(string baseURL); //, Action<TResultData> OnSuccess, Action<int,string> OnError);

   

    }



    public class ReqConfiguration : ReqBase<ReqConfigurationResp>
    {
        public override string endpoint { get; } = @"config.json";
      

        public void OnResponse(string jsonString)
        {
            ResultData = ResultData.NewFrom(jsonString);
        }

     



        public override void Setup(string rootUrl)
        {
            webRequest = UnityWebRequest.Get(rootUrl+"/"+ rootUrl);
            webRequest.downloadHandler = new DownloadHandlerBuffer();
           
        }


    }
}
