﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UiNs
{
    public class MainMenuButton : MonoBehaviour
    {
        public GameObject UiRoot;
        public Button menuButton;

        // Start is called before the first frame update
        void Start()
        {
            if (menuButton == null)
                menuButton = GetComponent<Button>();
            menuButton.onClick.AddListener(OnClick);
            UiRoot.SetActive(false);

        }

        void OnClick()
        {
            UiRoot.gameObject.SetActive(!UiRoot.gameObject.activeSelf);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}