﻿namespace Common
{
    public interface IStringId
    {
        string StringId { get; }
    }
}
