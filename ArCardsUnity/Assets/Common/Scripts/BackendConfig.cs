﻿using UnityEngine;

namespace Backend
{
    public enum BackendType
    {
        Undefined = 0,
        Development = 1,
        Production = 2,
        DevFileSystem = 3,
        Custom = 100,
        
    }

    static class BackendConstants
    {

        public const string BackendTypeKey = "BackendType";
        public const string CustomUrlKey = "CustomUrl";

        public const string Url_ProdServer = @"https://fourarcticfoxes.com/_DownloadableAssets/Knights";
        public const string Url_DevServer = @"https://fourarcticfoxes.com/_DownloadableAssets/DevKnights";

        public const string Url_DevFileSystem = @"e:\storage\_assets\Knights";

#if UNITY_EDITOR 
        public const BackendType DefaultBackendId = BackendType.DevFileSystem;
#else
        public const BackendType DefaultBackendId = BackendType.Production;
#endif
    }


    [System.Serializable]
    public class BackendConfigData
    {
        public BackendType CurBackendTypeId = BackendConstants.DefaultBackendId;
        public string CustomUrl;

        /// <summary>
        /// this field is exists to show in inspector only, it sgould not be used
        /// </summary>
        public string DbgCurBackendUrl = BackendConfig.DefaultBackendUrl;
        public string AsString()
        {
            return JsonUtility.ToJson(this, true);
        }

        public void Assign(BackendConfigData other)
        {
            CurBackendTypeId = other.CurBackendTypeId;
            DbgCurBackendUrl = other.DbgCurBackendUrl;
            CustomUrl = other.CustomUrl;
        }
    }



    /// <summary>
    /// Class usage:
    /// 1. Load data with Load
    /// 2. Read current data with Data
    /// 3. Do not modify Data object directly, use functions instead
    /// 4. Save data to persistant storage with Save()
    /// </summary>
    public static class BackendConfig
    {




        public static readonly string DefaultBackendUrl;

        /// <summary>
        /// puplic just for debug ourouses
        /// </summary>
        public static readonly BackendConfigData Data;
        public static void DbgOverrideData(BackendConfigData newData)
        {
            Data.Assign(newData);
            Save();
            MarkAsCustomBuild();
        }

        static public  bool IsProductionBackend { get { return Data.CurBackendTypeId == BackendType.Production; } }

        /// <summary>
        /// custom build is the build which built with additional dev tools even if it referes to production url
        /// </summary>
        static public  bool IsCustomBuild { get { return (_isCustomBuild || !IsProductionBackend); } }
        static bool _isCustomBuild = false;
        /// <summary>
        /// Mark this build as custom which has not be received by usual users
        /// </summary>
        public static void MarkAsCustomBuild()
        {
            _isCustomBuild = true;
        }

        static public bool IsLoaded { get; private set; }
        

        static BackendConfig()
        {
            DefaultBackendUrl = BackendConstants.Url_ProdServer;
            Data = new BackendConfigData();
        }

        static public string CurrentUrl
        {
            get
            {
                if (!IsLoaded)
                {
                    Debug.LogError("Attempt to access to BackendConfig when it was not loaded yet");
                    Load();
                }
                return GetUrlByBackendType(Data.CurBackendTypeId);
            }
        }

        static public string GetUrlByBackendType(BackendType typeId)
        {
            if( typeId == BackendType.Production )
                return BackendConstants.Url_ProdServer;

            if (typeId == BackendType.Development)
                return BackendConstants.Url_DevServer;

            if (typeId == BackendType.Custom)
                return CustomURL;

            if (typeId == BackendType.DevFileSystem)
                return BackendConstants.Url_DevFileSystem;

            Debug.LogError("Unknown BackendTypeId "+ typeId);
            return BackendConstants.Url_ProdServer;
        }

        static public void UpdateData(BackendType typeId)
        {
            Data.CurBackendTypeId = typeId;
            Data.DbgCurBackendUrl = GetUrlByBackendType(typeId);
           
        }



        public static  string CustomURL
        {
            get { return Data.CustomUrl; }
            set { Data.CustomUrl = value; }
        }


        static public void Load()
        {
            int backendTypeId = PlayerPrefs.GetInt(BackendConstants.BackendTypeKey, (int)BackendConstants.DefaultBackendId);
           
            if (!System.Enum.IsDefined(typeof(BackendType), backendTypeId) || backendTypeId <1)
            {
                Debug.Log("Undefined BackendTypeId in persistant " + backendTypeId+ ", using default "
                    + BackendConstants.DefaultBackendId);

                backendTypeId = (int) BackendConstants.DefaultBackendId;
            }
                      

            CustomURL = PlayerPrefs.GetString(BackendConstants.CustomUrlKey, DefaultBackendUrl);
            UpdateData((BackendType)backendTypeId);

            //Debug.Log(Data.AsString());
            IsLoaded = true;
        }


        static public void Save()
        {
            PlayerPrefs.SetInt(BackendConstants.BackendTypeKey, (int)Data.CurBackendTypeId);
            PlayerPrefs.SetString(BackendConstants.CustomUrlKey, CustomURL);
           
            PlayerPrefs.Save();
          
        }

    }
}
