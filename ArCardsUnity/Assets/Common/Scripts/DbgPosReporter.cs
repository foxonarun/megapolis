﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Common
{
    public class PosReporter : MonoBehaviour
    {
        public string TrackedObjName;
        public GameObject TrackedObj;
        public Text textField;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (TrackedObj != null && textField != null)
            {
                textField.text = string.Format
                    ("{0} p:{1} r:{2}",
                        TrackedObjName,
                         TrackedObj.transform.position,
                         TrackedObj.transform.rotation.eulerAngles
                    );
            }
        }
    }
}
