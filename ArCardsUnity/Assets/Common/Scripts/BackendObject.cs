﻿using Requests;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend
{
    public class BackendObject : MonoBehaviour
    {
        static BackendObject Instance;
        bool IsInitialized = false;

        [SerializeField]
        string dbgCurrentUrl;

        private void Awake()
        {
            if (Instance == null)
                Initialize();

        }

        private void Initialize()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            if (!BackendConfig.IsLoaded || !IsInitialized)
            {
                
                BackendConfig.Load();
                IsInitialized = true;
                dbgCurrentUrl = BackendConfig.CurrentUrl;
            }

        }
        private void OnDestroy()
        {
            Instance = null;
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        public static void Send()
        {
           var req = new ReqConfiguration();
            UnityWebRequest www = UnityWebRequest.Get("http://www.my-server.com");
        }
        

        // Update is called once per frame
        void Update()
        {

        }
    }
}
