﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    [System.Serializable]
    public class StringIdVal 
    {
        public string Id;
        public string Val;
    }
}
