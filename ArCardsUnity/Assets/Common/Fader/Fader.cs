using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Common
{
    public class Fader : MonoBehaviour
    {
        const string FaderObjectName = "Fader";
        
        bool isWorking = false;

        const float defaultVelocityRate = 4f;
        float velocityRate = defaultVelocityRate;
        
        string fadeScene;

        public float coverCurAlpha = 0.0f;
        Color fadeColor = Color.black;
        const float coverOpacity = 1;
        const float coverTransparent = 0;
        



        bool isHiding = false;

        AsyncOperation loadingOperation;
        bool isSceneActivationAllowed;
      //  public bool dbgDoSceneActivation;

        void AllowSceneActivation(bool isAllowed)
        {

            isSceneActivationAllowed = isAllowed;
            if (loadingOperation != null && loadingOperation.allowSceneActivation != isSceneActivationAllowed)
                loadingOperation.allowSceneActivation = isSceneActivationAllowed;
        }
        //Set callback
        void OnEnable()
        {
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }
        //Remove callback
        void OnDisable()
        {
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }

        

        #region Static functions
        public static Fader GetFader()
        {
            var ret = GameObject.FindObjectOfType<Fader>();
            if (ret == null)
            {
                GameObject newFaderObj = new GameObject();
                newFaderObj.name = FaderObjectName;
                ret = newFaderObj.AddComponent<Fader>();
                
            }
            DontDestroyOnLoad(ret);
            return ret;
        }

        public bool isNewSceneLoaded = false;
        void loadSceneAsync(string newSceneName, float hideTimeout, float damp)
        {
            if (isWorking)
            {
                Debug.LogError("Already in progress loading the scene " + fadeScene);
                return;
            }

          
            isWorking = true;
            isNewSceneLoaded = false;

            velocityRate = damp;
            fadeScene = newSceneName;

            StartCoroutine(UpdateVisibility(hideTimeout));


            loadingOperation = SceneManager.LoadSceneAsync(fadeScene);

            AllowSceneActivation(false);
          
        }

        Texture2D fadeTexture;
        
        IEnumerator UpdateVisibility(float timeOut)
        {
            if (fadeTexture == null)
                fadeTexture = new Texture2D(1, 1);

            coverCurAlpha = coverTransparent;
            fadeTexture.SetPixel(0, 0, fadeColor);
            fadeTexture.Apply();
            isHiding = true;
           

            yield return new WaitForSeconds(timeOut);

            bool isContinue = true;
            while (isContinue)
            {
                if (isHiding)
                    coverCurAlpha = Mathf.Lerp(coverCurAlpha, coverOpacity+0.1f, velocityRate * Time.smoothDeltaTime);
                else
                    coverCurAlpha = Mathf.Lerp(coverCurAlpha, coverTransparent-0.1f, velocityRate * Time.smoothDeltaTime);

                // if the cover 100% opsvity, i. e. scene is 100% hidden,
                // and new scene is already loaded, unhide the scene by making cover transparent again
                if (coverCurAlpha > coverOpacity && isHiding)
                {
                    if (!isNewSceneLoaded)
                    {
                       // if(dbgDoSceneActivation)
                            AllowSceneActivation(true);
                    }
                    else
                    {
                        isHiding = false;
                        coverCurAlpha = coverOpacity;
                        Debug.Log("scene loaded, started unhiding");
                    }

                }
                                
                if (coverCurAlpha < coverTransparent && !isHiding)
                {
                    isContinue = false;
                    coverCurAlpha = coverTransparent;
                    Debug.Log("unhiding finished");
                }

               
                yield return new WaitForSeconds(0.05f);
            }
            isWorking = false;
            Destroy(this.gameObject,1f);
        }

        public static Fader LoadSceneAsync(string newSceneName, float hideTimeout = 2, float damp = defaultVelocityRate)
        {
            var fader = GetFader();
            fader.loadSceneAsync(newSceneName, hideTimeout, damp);

            return fader;
        }

        #endregion

        void ApplyOpacity()
        {
            GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, coverCurAlpha);
            if (fadeTexture != null)
                GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
        }

        //Create a texture , Color it, Paint It , then Fade Away
        void OnGUI()
        {
           
            if (!isWorking)
                return;

            ApplyOpacity();
        }

        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {

            isNewSceneLoaded = true;
           // Debug.Log("--- scene "+scene.name+" is loaded ----");
        }

    }
}