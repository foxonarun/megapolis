﻿using VuStuff;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VuStuff
{
    public class VuToggleLightButton : MonoBehaviour
    {
        public Toggle toggleObj;
        VuCameraDevice _vuCameraDevice;
        VuCameraDevice vuCameraDevice
        {
            get
            {
                if (_vuCameraDevice == null)
                    _vuCameraDevice = FindObjectOfType<VuCameraDevice>();
                return _vuCameraDevice;
            }
        }


        // Use this for initialization
        void Start()
        {
            toggleObj.isOn = false;
            toggleObj.onValueChanged.AddListener(OnValueChanged);
        }

        

        void OnValueChanged(bool newVal)
        {
            Debug.Log("SwitchFlashTorch to "+newVal);
            if(vuCameraDevice != null)
                vuCameraDevice.SwitchFlashTorch(newVal);
        }


        IEnumerator SwitchFlashTorchOn()
        {
            if (vuCameraDevice == null)
            {
                toggleObj.interactable = false;
                toggleObj.isOn = false;
                Debug.Log("Camera device manager is not found");
                yield break;
            }

            yield return new WaitForSeconds(0.5f);
            yield return new WaitForEndOfFrame();
            vuCameraDevice.SwitchFlashTorch(toggleObj.isOn);

        }

        private void OnApplicationPause(bool isPause)
        {
            if (!isPause)
                StartCoroutine(SwitchFlashTorchOn());
             
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}