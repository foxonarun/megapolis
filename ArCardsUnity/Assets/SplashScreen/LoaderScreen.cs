﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scenes.LoaderScreen
{
    public class LoaderScreen : MonoBehaviour
    {
        public string NextSceneName = "MainMenu";
        public float timeOut = 3;
        // Use this for initialization
        IEnumerator Start()
        {
            yield return null;
           // yield return new WaitForSeconds(timeOut);
           Common.Fader.LoadSceneAsync(NextSceneName);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
