﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

namespace UiNs
{
    public class EditorModesPanel : MonoBehaviour
    {
        public GameObject PanelRoot;
        public Toggle GroundDetectionButton;
        public GameObject GroundDetectionPanel;
        public Button MainMenu;
        // Use this for initialization
        void Start()
        {
            PanelRoot.SetActive(false);
            GroundDetectionPanel.SetActive(false);



            AvtivateGroundDetection(false);


            GroundDetectionButton.onValueChanged.AddListener(AvtivateGroundDetection);
            MainMenu.onClick.AddListener(ToggleMainMenu);
        }

        void ToggleMainMenu()
        {
            bool isOn = !PanelRoot.activeSelf;
            PanelRoot.SetActive(isOn);
                   }

        public void AvtivateGroundDetection(bool isOn)
        {
            GroundDetectionPanel.SetActive(isOn);
        }


        void ActivateGroundDetectionMode()
        {
            GroundDetectionButton.isOn = true;
            AvtivateGroundDetection(GroundDetectionButton.isOn);


        }



        private void OnApplicationPause(bool pause)
        {
            if(!pause)
                ActivateGroundDetectionMode();
        }
        // Update is called once per frame
        void Update() {

        }
    }
}
