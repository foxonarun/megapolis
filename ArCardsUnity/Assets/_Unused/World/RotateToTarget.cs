﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldNs
{
    public class RotateToTarget : MonoBehaviour
    {

        public Transform RotatedObject;
        public Transform Target;

        public float SpeedRate = 45;

        // Start is called before the first frame update
        void Start()
        {
           

            if (RotatedObject == null)
                RotatedObject = transform;
           
        }

        // Update is called once per frame
        void LateUpdate()
        {
            Vector3 delta = Vector3.ProjectOnPlane(Target.position - RotatedObject.position, RotatedObject.up);


         

            Quaternion desiredRotation = Quaternion.LookRotation(delta, RotatedObject.up);
            
            //RotatedObject.rotation = desiredRotation;
            RotatedObject.rotation = Quaternion.RotateTowards(RotatedObject.rotation, desiredRotation, SpeedRate * Time.deltaTime);
        }

       
   
 



    }
}
