/*==============================================================================
Copyright (c) 2017-2018 PTC Inc. All Rights Reserved.

Vuforia is a trademark of PTC Inc., registered in the United States and other
countries.
==============================================================================*/

using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using WorldNs;

namespace FloorNs
{
    public enum PlaceModeId
    {
        Idle = 0,
        CorrectFloorHeight,
        PlaceWorldCenter
    }
    public class VuforiaFloorManager : MonoBehaviour
    {

        public PlaceModeId PlaceMode;

        const string unsupportedDeviceTitle = "Unsupported Device";
        const string unsupportedDeviceBody =
            "This device has failed to start the Positional Device Tracker. " +
            "Please check the list of supported Ground Plane devices on our site: " +
            "\n\nhttps://library.vuforia.com/articles/Solution/ground-plane-supported-devices.html";

        StateManager vuforiaStateManager;

        public GameObject World;

        public PlaneFinderBehaviour planeFinderBehaviour;
        PositionalDeviceTracker positionalDeviceTracker;
        public ContentPositioningBehaviour contentPositioningBehaviour;
        SmartTerrain smartTerrain;

                      

        #region MONOBEHAVIOUR_METHODS

        void Start()
        {


            VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
            VuforiaARController.Instance.RegisterOnPauseCallback(OnVuforiaPaused);
            DeviceTrackerARController.Instance.RegisterTrackerStartedCallback(OnTrackerStarted);
            DeviceTrackerARController.Instance.RegisterDevicePoseStatusChangedCallback(OnDevicePoseStatusChanged);


            InitPlaneFinder();
        }


        private void InitPlaneFinder()
        {
            if(planeFinderBehaviour == null)
                planeFinderBehaviour = FindObjectOfType<PlaneFinderBehaviour>();
            //planeFinderBehaviour.HitTestMode = HitTestMode.AUTOMATIC;

            if(contentPositioningBehaviour == null)
                contentPositioningBehaviour = planeFinderBehaviour.GetComponent<ContentPositioningBehaviour>();
            contentPositioningBehaviour.DuplicateStage = false;

            planeFinderBehaviour.OnInteractiveHitTest.AddListener(HandleInteractiveHitTest);
            planeFinderBehaviour.OnAutomaticHitTest.AddListener(HandleAutomaticHitTest);
        }

        void Update()
        {


        }

        void OnDestroy()
        {
            Debug.Log("OnDestroy() called.");

            VuforiaARController.Instance.UnregisterVuforiaStartedCallback(OnVuforiaStarted);
            VuforiaARController.Instance.UnregisterOnPauseCallback(OnVuforiaPaused);
            DeviceTrackerARController.Instance.UnregisterTrackerStartedCallback(OnTrackerStarted);
            DeviceTrackerARController.Instance.UnregisterDevicePoseStatusChangedCallback(OnDevicePoseStatusChanged);
        }

        #endregion // MONOBEHAVIOUR_METHODS


        #region GROUNDPLANE_CALLBACKS

        public void HandleInteractiveHitTest(HitTestResult result)
        {
            Vector3 pos = result.Position;
            Quaternion rot = result.Rotation;

            switch (PlaceMode)
            {
                case PlaceModeId.Idle:
                    break;

                case PlaceModeId.CorrectFloorHeight:

                    pos.x = World.transform.position.x;
                    pos.z = World.transform.position.z;

                    World.transform.position = pos;
                    PlaceMode = PlaceModeId.Idle;
                    break;

            }
            /*
            if (objectAligner != null)
            {
                objectAligner.Align();
            }
            */
            // Debug.Log("HandleAutomaticHitTest: position=" + result.Position + " rotation:" + result.Rotation);
        }

        public void PlaceWorldHeight()
        {
            PlaceAnchor();
            PlaceMode = PlaceModeId.CorrectFloorHeight;
        }

        public void HandleAutomaticHitTest(HitTestResult result)
        {
            Debug.Log("HandleAutomaticHitTest: position="+ result.Position+" rotation:"+ result.Rotation);
        }


        public void PlaceAnchor()
        {
            planeFinderBehaviour.PerformHitTest(Vector3.zero);
        }


        /*
        public void HandleInteractiveHitTest(HitTestResult result)
        {
            if (result == null)
            {
                Debug.LogError("Invalid hit test result!");
                return;
            }

           // if ( !m_GroundPlaneUI.IsCanvasButtonPressed())
            {
                Debug.Log("HandleInteractiveHitTest() called.");

                // If the PlaneFinderBehaviour's Mode is Automatic, then the Interactive HitTestResult will be centered.

                planeFinderBehaviour = FindObjectOfType<PlaneFinderBehaviour>();
                contentPositioningBehaviour = planeFinderBehaviour.GetComponent<ContentPositioningBehaviour>();
                contentPositioningBehaviour.DuplicateStage = false;

                // Place object based on Ground Plane mode
                switch (planeMode)
                {
                    case PlaneMode.GROUND:

                        contentPositioningBehaviour.AnchorStage = m_PlaneAnchor;
                        contentPositioningBehaviour.PositionContentAtPlaneAnchor(result);
                        UtilityHelper.EnableRendererColliderCanvas(m_PlaneAugmentation, true);

                        // Astronaut should rotate toward camera with each placement
                        m_PlaneAugmentation.transform.localPosition = Vector3.zero;
                        UtilityHelper.RotateTowardCamera(m_PlaneAugmentation);

                        AstronautIsPlaced = true;

                        break;

                    case PlaneMode.PLACEMENT:

                        if (!m_ProductPlacement.IsPlaced || TouchHandler.DoubleTap)
                        {
                            contentPositioningBehaviour.AnchorStage = m_PlacementAnchor;
                            contentPositioningBehaviour.PositionContentAtPlaneAnchor(result);
                            UtilityHelper.EnableRendererColliderCanvas(m_PlacementAugmentation, true);
                        }

                        if (!m_ProductPlacement.IsPlaced)
                        {
                            m_ProductPlacement.SetProductAnchor(m_PlacementAnchor.transform);
                            m_TouchHandler.enableRotation = true;
                        }

                        break;
                }
            }
        }

        */

        #endregion // GROUNDPLANE_CALLBACKS



        public void ResetTrackers()
        {
            Debug.Log("ResetTrackers() called.");

            smartTerrain = TrackerManager.Instance.GetTracker<SmartTerrain>();
            positionalDeviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();

            // Stop and restart trackers
            smartTerrain.Stop(); // stop SmartTerrain tracker before PositionalDeviceTracker
            positionalDeviceTracker.Reset();
            smartTerrain.Start(); // start SmartTerrain tracker after PositionalDeviceTracker
        }






        bool DoAnchorsExist()
        {
            if (vuforiaStateManager != null)
            {
                IEnumerable<TrackableBehaviour> trackableBehaviours = vuforiaStateManager.GetActiveTrackableBehaviours();

                foreach (TrackableBehaviour behaviour in trackableBehaviours)
                {
                    if (behaviour is AnchorBehaviour)
                    {
                        return true;
                    }
                }
            }
            return false;
        }




        #region VUFORIA_CALLBACKS

        void OnVuforiaStarted()
        {
            Debug.Log("OnVuforiaStarted() called.");

            vuforiaStateManager = TrackerManager.Instance.GetStateManager();

            // Check trackers to see if started and start if necessary
            positionalDeviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();
            smartTerrain = TrackerManager.Instance.GetTracker<SmartTerrain>();

            if (positionalDeviceTracker != null && smartTerrain != null)
            {
                if (!positionalDeviceTracker.IsActive)
                    positionalDeviceTracker.Start();
                if (positionalDeviceTracker.IsActive && !smartTerrain.IsActive)
                    smartTerrain.Start();
            }
            else
            {
                if (positionalDeviceTracker == null)
                    Debug.Log("PositionalDeviceTracker returned null. GroundPlane not supported on this device.");
                if (smartTerrain == null)
                    Debug.Log("SmartTerrain returned null. GroundPlane not supported on this device.");
                
                MessageBox.DisplayMessageBox(unsupportedDeviceTitle, unsupportedDeviceBody, false, null);
            }
        }

        

        void OnVuforiaPaused(bool paused)
        {
            Debug.Log("OnVuforiaPaused(" + paused.ToString() + ") called.");

            if (paused)
                ResetScene();
        }

        #endregion // VUFORIA_CALLBACKS

        /// <summary>
        /// set all augmentation items to initial positiona
        /// </summary>
        public void ResetScene()
        {
            Debug.Log("ResetScene() called.");

            // reset augmentations
            /*
            m_PlaneAugmentation.transform.position = Vector3.zero;
            m_PlaneAugmentation.transform.localEulerAngles = Vector3.zero;
            UtilityHelper.EnableRendererColliderCanvas(m_PlaneAugmentation, false);

            m_MidAirAugmentation.transform.position = Vector3.zero;
            m_MidAirAugmentation.transform.localEulerAngles = Vector3.zero;
            UtilityHelper.EnableRendererColliderCanvas(m_MidAirAugmentation, false);

            m_ProductPlacement.Reset();
            UtilityHelper.EnableRendererColliderCanvas(m_PlacementAugmentation, false);

            DeleteAnchors();
            m_ProductPlacement.SetProductAnchor(null);
            AstronautIsPlaced = false;
            m_GroundPlaneUI.Reset();
            m_TouchHandler.enableRotation = false;
            */
        }

        #region DEVICE_TRACKER_CALLBACKS

        void OnTrackerStarted()
        {
            Debug.Log("OnTrackerStarted() called.");

            positionalDeviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();
            smartTerrain = TrackerManager.Instance.GetTracker<SmartTerrain>();

            if (positionalDeviceTracker != null)
            {
                if (!positionalDeviceTracker.IsActive)
                    positionalDeviceTracker.Start();

                Debug.Log("PositionalDeviceTracker is Active?: " + positionalDeviceTracker.IsActive +
                          "\nSmartTerrain Tracker is Active?: " + smartTerrain.IsActive);
            }
        }

        void OnDevicePoseStatusChanged(TrackableBehaviour.Status status, TrackableBehaviour.StatusInfo statusInfo)
        {
            Debug.Log("OnDevicePoseStatusChanged(" + status + ", " + statusInfo + ")");
        }

        #endregion // DEVICE_TRACKER_CALLBACK_METHODS
    }
}